# Generated by Django 2.0.6 on 2018-08-30 05:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hall_layout', '0002_order_tabletoordermembership'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='name',
            field=models.CharField(default='yuriy', max_length=256),
            preserve_default=False,
        ),
    ]
