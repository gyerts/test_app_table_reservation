from rest_framework import serializers

from . import models


class CoordinatesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Coordinates
        fields = ('horizontal', 'vertical')


class SizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Size
        fields = ('width', 'height')


class TableSerializer(serializers.ModelSerializer):
    coordinates = CoordinatesSerializer()
    size = SizeSerializer()

    class Meta:
        model = models.Table
        fields = ('number', 'seats', 'shape', 'coordinates', 'size')
