from datetime import datetime
from . import models


def convert_date(date: str) -> datetime.date:
    """
    :param date: should be str in format 'yyyy.mm.dd'
    :return: datetime.date object
    """
    return datetime.strptime(date, '%Y.%m.%d').date()


def get_table_to_order_memberships(date: str) -> models.TableToOrderMembership:
    """
    :param date: should be str in format 'yyyy.mm.dd'
    :return: TableToOrderMemberships which have orders on requested date
    """
    return models.TableToOrderMembership.objects.filter(order__date=convert_date(date))


def get_order(date: str, email: str, name: str) -> models.Order:
    try:
        order = models.Order.objects.get(
            date=convert_date(date),
            email=email,
            name=name
        )
    except models.Order.DoesNotExist:
        order = models.Order.objects.create(
            date=convert_date(date),
            email=email,
            name=name,
        )
        order.save()
    return order
