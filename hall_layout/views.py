import json
from django.shortcuts import render

from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authentication import SessionAuthentication, BasicAuthentication

from django.views.decorators.csrf import csrf_exempt

from . import models
from . import serializers
from . import helpers


class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening


class AllTablesViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.TableSerializer
    queryset = models.Table.objects.all()

    def list(self, request):
        serializer = serializers.TableSerializer(models.Table.objects.all(), many=True)
        headers = self.get_success_headers(serializer.data)
        response = {"detail": "", "data": { "tables": serializer.data }}
        return Response(response, status=status.HTTP_200_OK, headers=headers)


class BusyTablesViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.TableSerializer
    queryset = models.TableToOrderMembership.objects.all()

    def list(self, request, date):
        busy_tables = [membership.table for membership in helpers.get_table_to_order_memberships(date)]
        serializer = serializers.TableSerializer(busy_tables, many=True)
        headers = self.get_success_headers(serializer.data)
        response = {"detail": "", "data": { "busy_tables": serializer.data }}
        return Response(response, status=status.HTTP_200_OK, headers=headers)


class ReserveView(APIView):
    authentication_classes = (CsrfExemptSessionAuthentication, BasicAuthentication)

    @staticmethod
    def validate_request_body(request) -> (bool, dict, ):
        errors = {"detail": list()}
        json_payload = None

        try:
            json_payload = json.loads(request.body.decode())
        except json.decoder.JSONDecodeError:
            errors["detail"].append("not readable json data")

        if json_payload:
            if 'email' not in json_payload: errors["detail"].append("no 'email' in json data")
            if 'name' not in json_payload: errors["detail"].append("no 'name' in json data")
            if 'tables' not in json_payload: errors["detail"].append("no 'tables' in json data")
            if 'date' not in json_payload: errors["detail"].append("no 'date' in json data")

        return (not bool(errors["detail"])), errors

    @csrf_exempt
    def post(self, request, *args, **kwargs):
        valid, errors = self.validate_request_body(request)

        if not valid:
            return Response(errors, status=status.HTTP_400_BAD_REQUEST)

        json_payload = json.loads(request.body.decode())

        tables_for_reservation = json_payload["tables"]
        date = json_payload["date"]

        reserved_tables = [membership.table for membership in helpers.get_table_to_order_memberships(date)]
        print(reserved_tables)
        for reserved_table in reserved_tables:
            print(reserved_table.number, " in ", tables_for_reservation)
            if reserved_table.number in tables_for_reservation:
                return Response({
                    "detail": "table already reserved",
                    "data": {
                        "table": reserved_table.number
                    }
                }, status=status.HTTP_400_BAD_REQUEST)

        order = helpers.get_order(date=date, email=json_payload["email"], name=json_payload["name"])

        for just_reserved_table_num in tables_for_reservation:
            just_reserved_table = models.Table.objects.get(number=just_reserved_table_num)

            models.TableToOrderMembership.objects.create(
                table=just_reserved_table,
                order=order,
            ).save()

        return Response({"detail": "tables registered"}, status=status.HTTP_200_OK)
