from django.db import models
from django.contrib import admin


class Coordinates(models.Model):
    horizontal = models.IntegerField(default=0, help_text="расположение в % по горизонтали")
    vertical = models.IntegerField(default=0, help_text="расположение в % по вертикали")


class Size(models.Model):
    width = models.IntegerField(default=1, help_text="ширина в % относительно зала")
    height = models.IntegerField(default=1, help_text="длина в % относительно зала")


class Table(models.Model):
    '''
    Схема зала - столы, у каждого из которых есть следующие свойства:
        номер
        количество мест
        форма (прямоугольный, овальный)
        координаты (по горизонтали, по вертикали) ?
        размеры (ширина, длина)
    '''

    RECTANGULAR = 'RECT'
    OVAL = 'OVAL'
    SHAPES = (
        (RECTANGULAR, 'Rectangular'),
        (OVAL, 'Oval'),
    )

    number = models.IntegerField(default=0, help_text="номер")
    seats = models.IntegerField(default=1, help_text="количество мест")
    shape = models.CharField(max_length=4, choices=SHAPES, default=RECTANGULAR)

    coordinates = models.ForeignKey('Coordinates', on_delete=models.CASCADE,)
    size = models.ForeignKey('Size', on_delete=models.CASCADE,)

    def __str__(self):
        return "table: %s" % self.number


class Order(models.Model):
    date = models.DateField()
    email = models.EmailField()
    name = models.CharField(max_length=256)

    def __str__(self):
        return "{} {} {}".format(self.date, self.name, self.email)


class TableToOrderMembership(models.Model):
    table = models.ForeignKey(Table, on_delete=models.CASCADE)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)

    def __str__(self):
        return "{} -> {}".format(self.table, self.order)


admin.site.register(Coordinates)
admin.site.register(Size)
admin.site.register(Table)
admin.site.register(Order)
admin.site.register(TableToOrderMembership)
