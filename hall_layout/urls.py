from django.conf.urls import url, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^get_tables', views.AllTablesViewSet.as_view({'get': 'list'})),
    url(r'^get_busy_tables/(?P<date>[0-9.]*)/$', views.BusyTablesViewSet.as_view({'get': 'list'})),
    url(r'^register_tables', views.ReserveView.as_view()),
]
